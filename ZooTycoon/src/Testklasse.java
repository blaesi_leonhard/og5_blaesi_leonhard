
public class Testklasse {
	public static void main(String[] args) {
		Addon tierfutter = new Addon();
		tierfutter.setAktuellerbestand(1);
		tierfutter.setBezeichnung("Bananen");
		tierfutter.setId_nummer(1);
		tierfutter.setMax_bestand(20);
		tierfutter.setVerkaufspreis(2.99);

		System.out.println("ID Nummer: "+ tierfutter.getId_nummer());
		System.out.println("Bezeichnung: "+ tierfutter.getBezeichnung());
		System.out.println("Maximaler Bestand: "+ tierfutter.getMax_bestand());
		System.out.println("Verkaufspreis: "+ tierfutter.getVerkaufspreis());
		System.out.println("Aktueller Bestand: "+ tierfutter.getAktuellerbestand());
	}
}
