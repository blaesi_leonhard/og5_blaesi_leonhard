public class Addon {

	private int id_nummer;
	private int max_bestand;
	private int aktuellerbestand;
	private String Bezeichnung;
	private double verkaufspreis;

	public Addon(){
		this.id_nummer = 0;
		this.max_bestand = 0;
		this.aktuellerbestand = 0;
		this.Bezeichnung = "";
		this.verkaufspreis = 0.00;
	}
	
	public double berechneGesamtwert (double preis){
		return preis*aktuellerbestand; 
	}
	
	public void aenderebestand (int neubestand){
		this.aktuellerbestand = aktuellerbestand + neubestand;
	}
	
	public int getId_nummer() {
		return id_nummer;
	}

	public void setId_nummer(int id_nummer) {
		this.id_nummer = id_nummer;
	}

	public int getMax_bestand() {
		return max_bestand;
	}

	public void setMax_bestand(int max_bestand) {
		this.max_bestand = max_bestand;
	}

	public int getAktuellerbestand() {
		return aktuellerbestand;
	}

	public void setAktuellerbestand(int aktuellerbestand) {
		this.aktuellerbestand = aktuellerbestand;
	}

	public String getBezeichnung() {
		return Bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		Bezeichnung = bezeichnung;
	}

	public double getVerkaufspreis() {
		return verkaufspreis;
	}

	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}

	public static void main(String[] args) {

	}
}
