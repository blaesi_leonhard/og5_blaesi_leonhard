
public class TestKickers {

	public static void main(String[] args) {

		Trainer koenig = new Trainer("Muhammed", "+490545564125", true, 'A', 125);

		System.out.println("Muhammed - " + koenig.getName());
		System.out.println("+490545564125 - " + koenig.getTelefonnummer());
		System.out.println("true - " + koenig.isjahresbeitragbezahlt());
		System.out.println("A - " + koenig.getLizenzklasse());
		System.out.println("125 - " + koenig.getMonatlicheAufwandentschädigung());

		Schiedsrichter seher = new Schiedsrichter("Allesseher", "+49597616531", true, 3);

		System.out.println("Allesseher - " + seher.getName());
		System.out.println("+49597616531 - " + seher.getTelefonnummer());
		System.out.println("true - " + seher.isjahresbeitragbezahlt());
		System.out.println("Allesseher - " + seher.getAnzahlSpiele());

		Spieler zm = new Spieler("Gomez", "+4918764237",false,14, "Zentral Mitte");
		
		System.out.println("Gomez - " +zm.getName());
		System.out.println("+4918764237 - " +zm.getTelefonnummer());
		System.out.println("false - " +zm.isjahresbeitragbezahlt());
		System.out.println("14 - " +zm.getTrikotnummer());
		System.out.println("Zentral Mitte - " +zm.getSpielposition());
		
		
		
		Mannschaftsleiter gomez = new Mannschaftsleiter("Gomez", "+4918764237",false,14, "Zentral Mitte","BVB",200);
		
		System.out.println("Gomez - " +gomez.getName());
		System.out.println("+4918764237 - " +gomez.getTelefonnummer());
		System.out.println("false - " +gomez.isjahresbeitragbezahlt());
		System.out.println("14 - " +gomez.getTrikotnummer());
		System.out.println("Zentral Mitte - " +gomez.getSpielposition());
		System.out.println("BVB - " +gomez.getNameMannschaft());
		System.out.println("200 - " +gomez.getRabattaufjahresbetrag());
		
		
		
	
	}
}
