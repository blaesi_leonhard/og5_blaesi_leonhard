
public abstract class Person {
   
	private String name;
	private String telefonnummer;
	private boolean jahresbeitragbezahlt;
	
	
	public Person(String name, String telefonnummer, boolean jahresbeitragbezahlt) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbeitragbezahlt = jahresbeitragbezahlt;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isjahresbeitragbezahlt() {
		return jahresbeitragbezahlt;
	}
	public void setjahresbeitragbezahlt(boolean jahresbeitragbezahlt) {
		this.jahresbeitragbezahlt = jahresbeitragbezahlt;
	}
	
}
