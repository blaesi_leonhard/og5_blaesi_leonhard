
public class Trainer extends Person {

	private char lizenzklasse;
	private int monatlicheAufwandentschädigung;
	
	
	public Trainer(String name, String telefonnummer, boolean jahresbeitragbezahlt, char lizenzklasse,int monatlicheAufwandentschädigung) {
		super(name, telefonnummer, jahresbeitragbezahlt);
		this.lizenzklasse = lizenzklasse;
		this.monatlicheAufwandentschädigung = monatlicheAufwandentschädigung;
	}
	
	public char getLizenzklasse() {
		return lizenzklasse;
	}
	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}
	public int getMonatlicheAufwandentschädigung() {
		return monatlicheAufwandentschädigung;
	}
	public void setMonatlicheAufwandentschädigung(int monatlicheAufwandentschädigung) {
		this.monatlicheAufwandentschädigung = monatlicheAufwandentschädigung;
	}
	
	
	
}
