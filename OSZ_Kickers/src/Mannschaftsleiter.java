
public class Mannschaftsleiter extends Spieler {

	private String nameMannschaft;
	private int rabattaufjahresbetrag;
	
	public Mannschaftsleiter(String name, String telefonnummer, boolean jahresbeitragbezahlt, int trikotnummer,
			String spielposition, String nameMannschaft, int rabattaufjahresbetrag) {
		super(name, telefonnummer, jahresbeitragbezahlt, trikotnummer, spielposition);
		this.nameMannschaft = nameMannschaft;
		this.rabattaufjahresbetrag = rabattaufjahresbetrag;
	}
	
	public String getNameMannschaft() {
		return nameMannschaft;
	}
	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}
	public int getRabattaufjahresbetrag() {
		return rabattaufjahresbetrag;
	}
	public void setRabattaufjahresbetrag(int rabattaufjahresbetrag) {
		this.rabattaufjahresbetrag = rabattaufjahresbetrag;
	}
}
