
public class Schiedsrichter extends Person {
	
	private int anzahlSpiele ;

	public Schiedsrichter(String name, String telefonnummer, boolean jahresbeitragbezahlt, int anzahlSpiele) {
		super(name, telefonnummer, jahresbeitragbezahlt);
		this.anzahlSpiele = anzahlSpiele;
	}

	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}

	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
	}
	

}
