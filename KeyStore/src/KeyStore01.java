import java.util.Arrays;

public class KeyStore01 {
	private String[] array;
	
	
	public KeyStore01(int length) {
		super();
		array = new String[length];
	}

	public KeyStore01() {
		super();
		array = new String[100];
	}

	public boolean add(java.lang.String e){
		for(int i = 0; i < array.length; i++){
			if(this.array[i] == null){
				this.array[i] = e;
				return true;
				}	
		}
		return false;	
	}
	
	public int indexOf(java.lang.String str){
		for(int i = 0; i< this.array.length; i++){
			if(this.array[i].equals(str)){
				return i;
			}
		}
		return -1;
	}
	
	public boolean remove(java.lang.String str){
		for(int i = 0; i< this.array.length; i++){
			if(this.array[i].equals(str)){
				this.array[i]=null;
				return true;
			}
		}
		return false;
	}
	
	public boolean remove(int index){
		if (index > this.array.length)
			return false;
		this.array[index] = null;
			return true;
	}

	@Override
	public String toString() {
		return "KeyStore01 [array=" + Arrays.toString(array) + "]";
	}
	
	
	
	

	
}
